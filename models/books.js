let BooksJSON =
    '[{"id":"book1","name":"Book Title","price":"9.99","url":"http://something.org/book"},' +
    '{"id":"book2","name":"Book Title 2","price":"1.99","url":"http://something.org/book2"}]';

class books{
    constructor(booksJSON){
        this.books = JSON.parse(booksJSON);
    };
    addBook(id, name, price, url) {
        this.books.push({id: id, name: name, price: price, url: url});
        return true;
    };
    deleteBook(id){
        for(let book in this.books){
            if(this.books[book].id === id){
                this.books.splice(book,book+1);
                console.log(this.books);
                return true;
            }
        }
        return false;
    };
}


booksObject = new books(BooksJSON);

module.exports = booksObject;