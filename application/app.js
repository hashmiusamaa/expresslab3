'use strict';

let express = require('express');
let app = express();
let landing_router = require('../routes/landing');
let login_router = require('../routes/login');
let list_router = require('../routes/list');
let purchase_router = require('../routes/purchase');
let confirm_router = require('../routes/confirm');
let logout_router = require('../routes/logout');
let existing_router = require('../routes/existing');
let admin_login_router = require('../routes/admin_login');
let manage_router = require('../routes/manage');

let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser')();
let session = require('express-session');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(cookieParser);
app.use(session({secret: 'alphabetagamma', resave: true, saveUninitialized: true}));
app.use((req, res, next)=>{
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});

app.use('/landing', landing_router);
app.use('//landing', landing_router);
app.use('/', login_router);
app.use('//', login_router);
app.use('/list', list_router);
app.use('//list', list_router);
app.use('/purchase', purchase_router);
app.use('//purchase', purchase_router);
app.use('/confirm', confirm_router);
app.use('//confirm', confirm_router);
app.use('/logout', logout_router);
app.use('/existing', existing_router);
app.use('/admin_login', admin_login_router);
app.use('/manage', manage_router);
app.all('*', (req, res)=>{ res.redirect('/landing')});

module.exports = app;