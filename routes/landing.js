'use strict';

let pug = require('pug');
let express = require('express');
let books = require('../models/books');
let users = require('../models/users');

let landing_router = express.Router();

landing_router.get('/',(req, res)=>{
    req.session.destroy();
    //console.log(users);
    res.send(pug.renderFile('./views/landing.pug', {books: books.books}));
});

module.exports = landing_router;