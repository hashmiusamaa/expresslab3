'use strict';

let express = require('express');
let purchase_router = express.Router();
let pug = require('pug');
let books = require('../models/books');

purchase_router.post('/',(req, res)=>{
    console.log('purchase router');
    console.log(req.session);
    console.log(req.body);
    req.session.current = '/purchase';
    if(req.session && (['list','purchase'].includes(req.session.from))){
        let quantity = req.body.Quantity;
        if (isNaN(quantity)){
            res.send(pug.renderFile('./views/list.pug', {quantity: quantity ,error: "Enter correct quantity", books: JSON.parse(books), name: req.session.name}));
        }
        let book_cart = [];
        let total_cost = 0;
        if(req.session.book_cart !== undefined && req.session.book_cart.length !== 0){
            console.log('Coming from previous work flow or refresh.');
            book_cart = req.session.book_cart;
            total_cost = req.session.total_cost;
        }else{
            books.books.forEach((book)=>{
                if(req.body.Books.includes(book.id)){
                    book_cart.push(
                        {
                            name: book.name,
                            quantity: quantity,
                            itemPrice: book.price,
                            totalPrice: book.price*quantity
                        });
                    total_cost += book.price*quantity;
                }
            });
        }

        req.session.book_cart = book_cart;
        req.session.total_cost = total_cost;
        req.session.from = 'purchase';
        res.send(pug.renderFile('./views/purchase.pug', {orders:book_cart, name: req.session.name, totalCost: total_cost}));
    }else{
        res.redirect('/landing');
    }

});


purchase_router.get('/',(req, res)=>{
    console.log(req.session.from);
    req.session.current = '/purchase';
    if(req.session && (['list','purchase'].includes(req.session.from))){
        res.send(pug.renderFile('./views/purchase.pug', {orders:req.session.book_cart, name: req.session.name, totalCost: req.session.total_cost}));
    }else{
        res.redirect('/landing');
    }

});

module.exports = purchase_router;