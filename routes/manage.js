'use strict';

let express = require('express');
let pug = require('pug');
let admins = require('../models/admins');
let books = require('../models/books');

let manage_router = express.Router();

manage_router.get('/',(req, res)=>{
    console.log('manage get');
    if(req.session.admin && admins[req.session.admin.id]){
        res.send(pug.renderFile('./views/manage.pug'));
    }else{
        res.redirect('landing');
    }
});

manage_router.post('/',(req, res)=>{
    console.log('manage post');
    if(req.session.admin && admins[req.session.admin.id]){
        if(req.body.id && req.body.name){
            console.log('add called');
            books.addBook(req.body.id, req.body.name,req.body.price, req.body.url);
            res.send(pug.renderFile('./views/manage.pug', {message: "Book with id "+ req.body.id + " added."}));
        }else if(req.body.id){
            console.log('delete called');
            if(books.deleteBook(req.body.id)){
                res.send(pug.renderFile('./views/manage.pug', {message: "Book with id "+ req.body.id + " deleted."}));
            }else{
                res.send(pug.renderFile('./views/manage.pug', {message: "Book with id "+ req.body.id + " was not found."}));
            }
        }else{
            res.send(pug.renderFile('./views/manage.pug'));
        }
    }else{
        res.redirect('landing');
    }
});


module.exports = manage_router;