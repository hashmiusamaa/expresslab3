'use strict';

let express = require('express');
let pug = require('pug');
let books = require('../models/books');

let confirm_router = express.Router();

confirm_router.post('/',(req, res)=>{
    req.session.current = '/confirm';
    if(req.session && ['purchase', 'confirm'].includes(req.session.from)){
        req.session.from = 'confirm';
        req.session.book_cart = [];
        req.session.body = req.body;
        res.send(pug.renderFile('./views/confirm.pug', {card: req.body, session: req.session}));
    }else{
        res.redirect('/landing');
        //res.send('invalid request. <a href="/login.html">back to login</a>');
    }
});

confirm_router.get('/',(req, res)=>{
    req.session.current = '/confirm';
    if(req.session && ['purchase', 'confirm'].includes(req.session.from)){
        req.session.from = 'confirm';
        req.session.book_cart = [];
        res.send(pug.renderFile('./views/confirm.pug', {card: req.session.body, session: req.session.session}));
    }else{
        res.redirect('/landing');
        //res.send('invalid request. <a href="/login.html">back to login</a>');
    }
});

module.exports = confirm_router;