'use strict';
let port = 8080;
let app = require('./application/app');

app.listen(port, (err)=>{
    if(err){
        console.log(err);
    }else{
        console.log("Server is listening at port " + port);
    }
});